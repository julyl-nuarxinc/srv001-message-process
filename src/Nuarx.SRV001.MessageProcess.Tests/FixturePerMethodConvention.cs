﻿using System.Linq;
using Fixie;
using Nuarx.SRV001.Service.MessageProcessor.Tests.Helpers;

namespace Nuarx.SRV001.Service.MessageProcessor.Tests
{
    public class FixturePerMethodConvention : Convention
    {
        public FixturePerMethodConvention()
        {
            this.Classes
                .NameEndsWith("Tests")
                .Where(t => t.GetConstructors()
                    .All(ci => ci.GetParameters().Length == 0));

            this.Methods.Where(mi => mi.IsPublic && mi.IsVoid());
        }
    }
}