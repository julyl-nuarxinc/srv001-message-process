﻿using System.Collections.Generic;
using Nuarx.FND003.Messaging.Interfaces;
using Nuarx.SRV001.Service.MessageProcessor.Models;
using Newtonsoft.Json;
using Nuarx.FND003.Messaging.RabbitMq;
using Nuarx.FND003.Messaging.RabbitMq.Helpers;

namespace Nuarx.SRV001.Service.MessageProcessor.Tests
{
    public class MessageProcessorTests
    {
        public void Should_NotThrowException_When_EmailMessageIsQueued()
        {
            IMessageSender messageSender = new RabbitMqMessageSender.MessageSender(new RabbitMqConnectionFactory(), new RabbitMqMessageSender.MessageSenderSettings());

            var message = new Message
            {
                Key = "TestKey",
                Recipient = "julyl@nuarxinc.com",
                Values = new Dictionary<string, string>()
            };
            message.Values.Add("Email", "julyl@nuarxinc.com");
            
            messageSender.SendAsync("OnPasswordResetRequested", JsonConvert.SerializeObject(message));
        }

        public void Should_NotThrowException_When_TextMessageIsQueued()
        {
            IMessageSender messageSender = new RabbitMqMessageSender.MessageSender(new RabbitMqConnectionFactory(), new RabbitMqMessageSender.MessageSenderSettings());

            var message = new Message
            {
                Key = "TestKey",
                Recipient = "8103488349",
                Values = new Dictionary<string, string>()
            };
            message.Values.Add("Phone", "8103488349");

            messageSender.SendAsync("OnPasswordResetRequested", JsonConvert.SerializeObject(message));
        }
    }
}