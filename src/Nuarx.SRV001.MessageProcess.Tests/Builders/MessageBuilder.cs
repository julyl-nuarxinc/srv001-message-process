﻿using System;
using System.Collections.Generic;
using Nuarx.SRV001.Service.MessageProcessor.Models;

namespace Nuarx.SRV001.Service.MessageProcessor.Tests.Builders
{
    public class MessageBuilder
    {
        private string _key;
        private string _recipient;
        private Dictionary<string, string> _values;

        public Message Build()
        {
            return new Message
            {
                Key = _key,
                Recipient = _recipient,
                Values = _values
            };
        }

        public MessageBuilder()
        {
            this.Initialize();
        }

        public MessageBuilder Initialize()
        {
            return this;
        }

        public MessageBuilder WithKey(string value)
        {
            _key = value;

            return this;
        }

        public MessageBuilder WithRecipient(string value)
        {
            _recipient = value;

            return this;
        }

        public MessageBuilder WithValues(Dictionary<string, string> value)
        {
            _values = value;

            return this;
        }
    }
}