﻿using System;
using Fixie;

namespace Nuarx.SRV001.Service.MessageProcessor.Tests.Helpers
{
    public class CreateExchange : FixtureBehavior, ClassBehavior
    {
        public void Execute(Fixture context, Action next)
        {
            Create();
            next();
        }

        public void Execute(Class context, Action next)
        {
            Create();
            next();
        }

        private static void Create()
        {
        }
    }
}