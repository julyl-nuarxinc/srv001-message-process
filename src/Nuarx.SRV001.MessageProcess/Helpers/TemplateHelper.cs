﻿using System;
using System.Linq;
using DotLiquid;

namespace Nuarx.SRV001.Service.MessageProcessor.Helpers
{
    public static class TemplateHelper
    {
        public static void RegisterSafeType(Type type)
        {
            Template.RegisterSafeType(type,
               type
               .GetProperties()
               .Select(p => p.Name)
               .ToArray());
        }

        public static void RegisterSafeTypes(Type rootType)
        {
            rootType
               .Assembly
               .GetTypes()
               .Where(t => t.Namespace == rootType.Namespace)
               .ToList()
               .ForEach(RegisterSafeType);
        }
    }
}