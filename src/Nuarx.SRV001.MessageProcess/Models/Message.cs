﻿using System.Collections.Generic;

namespace Nuarx.SRV001.Service.MessageProcessor.Models
{
    public class Message
    {
        public string Key { get; set; }
        public string Recipient { get; set; }
        public Dictionary<string,string> Values { get; set; }
    }
}