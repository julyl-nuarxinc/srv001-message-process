﻿namespace Nuarx.SRV001.Service.MessageProcessor.Models
{
    public class TextMessage
    {
        public string To { get; set; }
        public string Body { get; set; }
    }
}