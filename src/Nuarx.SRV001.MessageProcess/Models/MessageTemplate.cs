﻿using System;
using Nuarx.PRD003.Shared;

namespace Nuarx.SRV001.Service.MessageProcessor.Models
{
    public class MessageTemplate
    {
        public Guid Id { get; set; }
        public string Key { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Placeholders { get; set; }
        public TemplateMessageType Type { get; set; }
    }
}