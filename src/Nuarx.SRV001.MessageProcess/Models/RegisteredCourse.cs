﻿namespace Nuarx.SRV001.Service.MessageProcessor.Models
{
    public class RegisteredCourse
    {
        public string ParticipantEmail { get; set; }
        public string ParticipantPhone { get; set; }
        public string ParticipantDisplayName { get; set; }
        public string ParticipantLoginName { get; set; }
        public string RegistrationCode { get; set; }
        public string CourseName { get; set; }
        public string RegisteredCourseLink { get; set; }
    }
}