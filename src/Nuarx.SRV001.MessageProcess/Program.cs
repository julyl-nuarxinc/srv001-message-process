﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace Nuarx.SRV001.Service.MessageProcessor
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<ProgramService>(s =>
                {
                    s.ConstructUsing(() => new ProgramService());
                    s.WhenStarted(programService => programService.Start());
                    s.WhenStopped(programService => programService.Stop());
                });

                x.RunAsLocalSystem();

                x.SetServiceName("Nuarx.SRV001.MessageProcessor");
                x.SetDisplayName("Nuarx.SRV001.MessageProcessor");
            });
        }
    }
}
