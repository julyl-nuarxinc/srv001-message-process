﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Text;
using Dapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nuarx.FND001.Core.Helpers;
using Nuarx.FND003.Messaging.Interfaces;
using Nuarx.FND003.Messaging.RabbitMq;
using Nuarx.FND003.Messaging.RabbitMq.Helpers;
using Nuarx.PRD003.Shared;
using Nuarx.SRV001.Service.MessageProcessor.Models;
using RabbitMQ.Client.Events;
using Nuarx.FND006.Service;

namespace Nuarx.SRV001.Service.MessageProcessor
{
    public class ProgramService : NuarxConsumerService
    {
        private readonly IMessageSender _messageSender;
        public override string Exchange => ConfigHelper.GetSettingAsString("nuarx:SRV001:Exchange");
        public override string Queue => ConfigHelper.GetSettingAsString("nuarx:SRV001:Queue");
        public override string RoutingKey => ConfigHelper.GetSettingAsString("nuarx:SRV001:RoutingKey");
        
        public ProgramService()
        {
            _messageSender = new RabbitMqMessageSender.MessageSender(new RabbitMqConnectionFactory(), new RabbitMqMessageSender.MessageSenderSettings());
        }

        public override bool Process(BasicDeliverEventArgs args)
        {
            var json = JsonConvert.DeserializeObject<dynamic>(
                Encoding.UTF8.GetString(args.Body));

            var templateKey = (string) json.Key;
            var recipient = (string)json.Recipient;
            var values = ((IEnumerable<KeyValuePair<string, JToken>>)json.Values)
                       .ToDictionary(kvp => kvp.Key, kvp => (object)kvp.Value);
            
            var template = GetByKey(templateKey);
            var dotTemplate = DotLiquid.Template.Parse(template.Body);
            template.Body = dotTemplate.Render(DotLiquid.Hash.FromDictionary(values));

            if (template.Type == TemplateMessageType.Email)
            {
                TrySendParticipantEmailMessage(recipient, template.Body, template.Subject);
            }
            else
            {
                TrySendParticipantTextMessage(recipient, template.Body);
            }
            
            return true;
        }

        public MessageTemplate GetByKey(string key)
        {
            const string sql = @"
                SELECT
                    t1.*
                FROM
                    [ProductContext].[MessageTemplate] t1
                WHERE
                    t1.[Key] = @Key";

            using (var cn = new SqlConnection(
                ConfigurationManager.ConnectionStrings["nuarx:DAT001"].ConnectionString))
            {
                var sqlResults = cn.Query<MessageTemplate>(sql, new { Key = key });

                return sqlResults.FirstOrDefault();
            }
        }

        public void TrySendParticipantTextMessage(string recipient, string body)
        {
            if (string.IsNullOrWhiteSpace(recipient))
            {
                return;
            }

            var message = new TextMessage
            {
                To = recipient,
                Body = body
            };

            _messageSender.SendAsync("OnTextSend", JsonConvert.SerializeObject(message));
        }

        public void TrySendParticipantEmailMessage(string recipient, string body, string subject)
        {
            if (string.IsNullOrWhiteSpace(recipient))
            {
                return;
            }

            var dto = new MailMessage();

            dto.To.Add(new MailAddress(recipient));
            dto.Subject = subject;
            dto.Body = body;
            dto.IsBodyHtml = true;
            dto.From = new MailAddress("devteam@nuarxinc.com");

            _messageSender.SendAsync("OnMailSend", JsonConvert.SerializeObject(dto));
        }
    }
}